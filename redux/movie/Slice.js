import { createSlice } from "@reduxjs/toolkit"

const initialState = {
  movies: [],
  movie: null,
  error: null,
  success: null,
  listLoading: false,
  actionLoading: false

}
export const callTypes = {
  list: "list",
  action: "action",
};

export const moviesSlice = createSlice({
  name: "Movies",
  initialState: initialState,
  reducers: {
    
    catchError: (state, action) => {
      state.error = action.payload.error;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },

    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },

    movieFetched: (state, action) => {
      const { entity } = action.payload
      state.actionsLoading = false;
      state.movie = entity;
      state.error = null;
    },

    moviesFetched: (state, action) => {
      const { entities } = action.payload
      state.listLoading = false;
      state.error = null;
      state.movies = entities;
    }
  },
});
