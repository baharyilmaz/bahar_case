import * as requestToServer from "./Crud"
import { moviesSlice, callTypes } from "./Slice"

const { actions } = moviesSlice

export const fetchAllMovies = ({ searchText }) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }))

  return requestToServer
    .getAllMovies()
    .then(response => {
      let movies = response.data
      if (searchText) {
        const regEx = new RegExp(`^${searchText}`, "ig");
        movies = movies.filter((movie) => movie.name.match(regEx));
      }
      dispatch(actions.moviesFetched({ entities: movies }))
    })
    .catch(error => {
      let errList = []
      dispatch(actions.catchError({ error: errList, callType: callTypes.list }));
    })
}

export const fetchMovieById = (data) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }))
  return requestToServer
    .getMovieById(data)
    .then(response => {
      dispatch(actions.movieFetched({ entity: response.data }))
    })
    .catch(error => {
      let errList = []
      dispatch(actions.catchError({ error: errList, callType: callTypes.action }));
    })
}
