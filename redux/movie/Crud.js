import axios from "axios"

const BASE_URL = `http://api.tvmaze.com`

export function getAllMovies() {
  return axios.get(`${BASE_URL}/shows`)
}

export function getMovieById({movieId}) {
  return axios.get(`${BASE_URL}/shows/${movieId}`)
}
