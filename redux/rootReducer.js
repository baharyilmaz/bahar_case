import {moviesSlice} from './movie/Slice';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
    movies: moviesSlice.reducer
});

export default rootReducer;
