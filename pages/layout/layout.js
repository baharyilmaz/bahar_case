import Header from "./components/header"
import styles from '../../styles/Main.module.css'

function Layout({ children }) {
    return (
        <div  >
            <Header className={styles.header} />
            <div align="center" className={styles.container}>
                {children}
            </div>
        </div>
    )
}

export default Layout;