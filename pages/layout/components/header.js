import React, { useEffect, useState, useRef } from 'react';
import { fetchAllMovies } from "../../../redux/movie/Actions"
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { useRouter } from 'next/router'
import styles from '../../../styles/Main.module.css'
import headerStyles from '../../../styles/Header.module.scss'
import { FaHome } from "react-icons/fa";

const Header = () => {

    const dispatch = useDispatch()
    const router = useRouter()
    const wrapperRef = useRef(null);
    const [searchKey, setSearchKey] = useState("")
    const [showSuggestions, setShowSuggestions] = useState(false)

    const { moviesList, listLoading } = useSelector(({ movies: movies }) => ({
        moviesList: movies.movies,
        listLoading: movies.listLoading,

    }), shallowEqual)

    const onChangeSearchInput = (e) => {
        const lastInput = e.nativeEvent.data // null ise silme işlemi
        const searchValue = e.target.value
        setSearchKey(searchValue)

        if (searchValue.length >= 3) {
            dispatch(fetchAllMovies({ searchText: searchValue })).then(() => setShowSuggestions(true)
            )
        }
        if (searchValue.length == 2 && lastInput === null) {
            dispatch(fetchAllMovies({ searchText: "" }))
            setShowSuggestions(false)
        }
    }

    //handleClickOutside of suggestionList
    useEffect(() => {
        document.addEventListener("click", handleClickOutside, false);
        return () => {
            document.removeEventListener("click", handleClickOutside, false);
        };
    }, []);

    const handleClickOutside = event => {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setShowSuggestions(false);
        }
    };


    return (
        <div className={headerStyles.header}>

            <div className={`col-lg-9 col-md-6 pl-5 ${headerStyles.bounceIn}`}>
                <div className="d-flex flex-row">
                    <div 
                        onClick={() => router.push(`/film`)}
                    >
                        <FaHome className={headerStyles.button} />
                    </div>
                    <div className="m-1">
                        <div> World of Movie</div>
                        <div className={styles.description}>Created by Bahar</div>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-6">
                <div className={headerStyles.search} ref={wrapperRef}>
                    <div className={headerStyles.searchContent} >

                        <input
                            type="text"
                            className={headerStyles.searchInput}
                            name="searchKey"
                            placeholder="Search by name"
                            value={searchKey}
                            onChange={(e) => {
                                onChangeSearchInput(e)
                            }}
                        />
                        {
                            showSuggestions && moviesList && (
                                <div className={headerStyles.suggestionList} >

                                    {moviesList.map((item) =>
                                        <p
                                            key={item.id}
                                            onClick={() => {
                                                router.push(`/film/${item.id}`)
                                            }} >
                                            {item.name}
                                        </p>
                                    )}
                                </div>
                            )
                        }

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header;
