import React, { useEffect } from 'react';
import Link from "next/link"
import { useRouter } from 'next/router'
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { fetchAllMovies } from "../../redux/movie/Actions"
import { Col, Row, Spinner, Button } from "react-bootstrap";
import styles from '../../styles/Main.module.css'
import { FaArrowUp } from "react-icons/fa";

const Movies = () => {

    const dispatch = useDispatch()
    const router = useRouter()

    const { moviesList, listLoading } = useSelector(({ movies: movies }) => ({
        moviesList: movies.movies,
        listLoading: movies.listLoading,

    }), shallowEqual)


    useEffect(() => {
        dispatch(fetchAllMovies(""))
    }, []);

    const scrollTop = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth"
        })
    }

    const goToMovieDetail = (movieId) => {
        router.push(`/film/${movieId}`)
    }

    return (
        <div >
            {
                listLoading ?
                    <Spinner animation="border" />
                    : <Row className={styles.movieList}>

                        {
                            moviesList.length ? moviesList.map((item) =>

                                <Col key={item.id} lg="auto" md="auto" onClick={() => goToMovieDetail(item.id)}>

                                    <div className={styles.card}>
                                        <div className="image">
                                            <img style={{ height: "40vmin" }} className={styles.image} alt="Fotoğraf Bulunamadı" draggable="false" src={item.image.medium}
                                            />
                                        </div>
                                        <div>
                                            {item.name}
                                        </div>
                                        <div>
                                            {item.rating.average}/10
                                </div>
                                    </div>

                                </Col>


                            )
                                : <h3>No movie found...</h3>
                        }
                        <Button
                            variant="outline-primary"
                            className={styles.btnScrollTop}
                            onClick={scrollTop}>

                            <FaArrowUp size={50} />
                        </Button>

                    </Row>
            }
        </div>
    )
}

Movies.getInitialProps = ({ store }) => { }

export default Movies