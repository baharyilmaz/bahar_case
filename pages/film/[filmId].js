import React, { useEffect } from 'react';
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { fetchMovieById } from "../../redux/movie/Actions";
import { useRouter } from 'next/router'
import styles from '../../styles/Main.module.css'
import { Col, Row, Spinner } from "react-bootstrap";

const FilmDetay = () => {
    const dispatch = useDispatch()
    const router = useRouter()
    const { filmId } = router.query

    const { movieDetail, actionsLoading } = useSelector(({ movies: movies }) => ({
        movieDetail: movies.movie,
        actionsLoading: movies.actionsLoading

    }), shallowEqual)

    useEffect(() => {
        dispatch(fetchMovieById({ movieId: filmId }))
    }, [router]);

    return (
        <div>

            {
                actionsLoading ?
                    <Spinner animation="border" />
                    :

                    movieDetail ? (
                        <div>
                            <h1>Movie Detail</h1>
                            <div className={styles.cardSecondary}>
                                <Row>
                                    <Col lg="auto" md="auto">
                                        <div >
                                            <img style={{ height: "50vmin" }} className={styles.image} alt="Movie Photo" draggable="false" src={movieDetail.image.medium}
                                            />
                                        </div>
                                    </Col>
                                    <Col lg={8} md="auto" >
                                        <div>
                                            <span className={styles.title}>Name:</span> {" "}{movieDetail.name}
                                        </div>
                                        <div>
                                            <span className={styles.title}>Rating:</span>{" "} {movieDetail.rating.average}/10
                                </div>
                                        <div>
                                            <span className={styles.title}>Genres:</span>{" "}{
                                                movieDetail.genres.map((genre) =>
                                                    <span className={styles.labelDefault}>{genre}{" "}</span>
                                                )
                                            }
                                        </div>
                                        <div className="mt-2">
                                            <span className={styles.title}>Summary:</span>{" "}
                                            <div

                                                dangerouslySetInnerHTML={{
                                                    __html: movieDetail.summary
                                                }}
                                            ></div>

                                        </div>
                                        {
                                            movieDetail.officialSite && (
                                                <div>
                                                    <a href={movieDetail.officialSite}>OfficialSite</a>
                                                </div>
                                            )
                                        }

                                        <div>
                                            <span className={styles.title}>Premiered Date:</span>{" "}{movieDetail.premiered}
                                        </div>
                                        <div className="mt-2">
                                            <span
                                                className={`${movieDetail.status === "Running" ? styles.labelSuccess : styles.labelDanger}`}>
                                                {movieDetail.status}
                                            </span>
                                        </div>
                                    </Col>
                                </Row>

                            </div>
                        </div>
                    )
                        : <h3>No movie found...</h3>

            }
        </div>
    )
}

export default FilmDetay