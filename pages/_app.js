import React from "react";
import { Provider } from "react-redux";
import App from "next/app";
import store from "../redux/store"
import Layout from "./layout/layout";
import withRedux from "next-redux-wrapper";
import  '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
class MyApp extends App {

  static async getInitialProps({ Component, ctx }) { //wait for rendering
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

    return { pageProps: pageProps };
  }
  render() {
    const { Component, pageProps } = this.props;
    return (
      <React.Fragment >
        <Provider store={store}>
            <Layout >
              <Component {...pageProps} />
            </Layout>
        </Provider>
      </React.Fragment>
    );
  }
}

//makeStore returns a new store for every request
const makeStore = () => store;

//withRedux wrapper that passes the store to the App Component
export default withRedux(makeStore)(MyApp);

